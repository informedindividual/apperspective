module.exports = [
  require("./make-webpack-config")({
    separateStylesheet: false,
    outputPath: '../../src/main/resources/assets/apps/apprspctiv',
    minimize: true,
    devtool: "source-map"
  })
];
