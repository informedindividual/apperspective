module.exports = [
  require("./make-webpack-config")({
    separateStylesheet: false,
    outputPath: 'public',
    minimize: false,
    devtool: "source-map"
  })
];
