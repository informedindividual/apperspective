#Develop locally (watches changes)
npm start

#Build for production
npm run build

##node / npm versions (using nvm)
node -v && npm -v
v7.6.0
4.1.2

##Problems with whatwg-fetch, rxjs 5.0 and typesript 2.0 typings
Read migration guide here:
https://github.com/ReactiveX/rxjs/blob/master/MIGRATION.md#subscription-dispose-is-now-unsubscribe

This explains the issue
https://github.com/DefinitelyTyped/DefinitelyTyped/issues/10919
.\node_modules\.bin\typings install --save --global dt~core-js
typings install dt~whatwg-streams --global --save