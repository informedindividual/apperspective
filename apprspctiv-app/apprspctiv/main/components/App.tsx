/* global FB */
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';
import Header from "../../scannables/components/Header";
import ScanLocationsLeaflet from "../../scannables/components/ScanLocationsLeaflet";
import LeafletControlButtons from "../../scannables/components/LeafletControlButtons";
import MyScannablesList from "../../scannables/components/MyScannablesList";
import "../../style/GoldenForms.css";


interface AppProps {
  dispatch: Dispatch<{}>;
}

class App extends React.Component<AppProps, void> {

    handleSave(text: string) {

    }

  render() {
    const { dispatch } = this.props;

    return (
      <div className="todoapp">
        <input type="hidden" id="facebookUserId"/>
          <Header/>
          <MyScannablesList />
          {/*<MyScannablesList onSave={this.handleSave.bind(this)}/>
          <LeafletControlButtons onSave={this.handleSave.bind(this)} />*/}
          {/*<ScanLocationsLeaflet onSave={this.handleSave.bind(this)}/>*/}
      </div>
    );
  }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(App);
