import { createAction, Action } from 'redux-actions';
import { assign } from 'lodash';

import { Scannable } from './model';

export const EDIT_SCANNABLE = 'EDIT_SCANNABLE';


const editScannable = createAction<Scannable, Scannable, string>(
  EDIT_SCANNABLE,
  (scannable: Scannable, newText: string) => <Scannable>assign(scannable, { text: newText })
);

export {
    editScannable
}
