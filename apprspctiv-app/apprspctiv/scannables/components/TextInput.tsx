import * as React from "react";
import config from "./../../config";
import "whatwg-fetch";
import {Observable} from "@reactivex/rxjs";

interface TextInputProps {
    onSave: (text: string)=>void;
    text?: string;
    placeholder?: string,
    editing?: boolean;
}
interface TextInputState {
    text: string;
}

let searchArea;

class TextInput extends React.Component<TextInputProps, TextInputState> {

    constructor(props, context) {
        super(props, context);
        this.state = {
            text: this.props.text || ''
        };
    }

    handleSubmit(e) {
        console.log("handleSubmit" + window["fbId"]);
        const text = e.target.value.trim();
        if (e.which === 13) {
            this.props.onSave(text);
        }
    }

    handleChange(e) {
        let apiService = config.apiService + '/scannables';
        console.log("handleChange" + window["fbId"]);
        this.setState({text: e.target.value});
        if (searchArea === undefined) {
            console.log('add observable');
            searchArea = document.querySelector('.opdataSearchInput');
            console.log('apiService:' + apiService);

            let source = Observable.create(function (observer) {
                fetch(apiService)
                    .then(res => res.json())
                    .then(j => {
                        observer.next(j);
                        observer.complete();
                    }).catch(observer.onError);
            });

            let oneSubscription = source.subscribe({
                next: x => console.log(x),
                error: e => console.error(e),
                complete: () => console.log('complete')
            });


            /*let requestStream = Observable.of(apiService);
             var responseStream = requestStream
             .flatMap(function(requestUrl) {
             //return Observable.fromPromise(jQuery.getJSON(requestUrl));
             });

             responseStream.subscribe(function(response) {
             // render `response` to the DOM however you wish
             });
             */
        }

    }

    handleBlur(e) {
        console.log("handleBlur" + window["fbId"]);
        this.props.onSave(e.target.value);
    }

    render() {
        return (
            <input className="opdataSearchInput"
                   type="text"
                   placeholder={this.props.placeholder}
                   autoFocus={true}
                   value={this.state.text}
                   onBlur={this.handleBlur.bind(this)}
                   onChange={this.handleChange.bind(this)}
                   onKeyDown={this.handleSubmit.bind(this)}/>
        );
    }
}


export default TextInput;
