import * as React from 'react';
import * as classNames from 'classnames';

import { Scannable } from '../model';

interface ScannableProps {
  scannable: Scannable;
}
interface ScannableState {
  selected: boolean;
};

class ScannableItem extends React.Component<ScannableProps, ScannableState> {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selected: false
    };
  }

  handleDoubleClick() {
    this.setState({ selected: true });
  }


  render() {
    const {scannable} = this.props;

    let element = (
        <div className="view">
          <label onDoubleClick={this.handleDoubleClick.bind(this)}>
            {scannable.gtin}
          </label>
        </div>
      );


    return (
      <li>
        {element}
      </li>
    );
  }
}

export default ScannableItem;
