import * as React from "react";
import "../../style/LeafletControlButtons.css";
import {Observable} from "@reactivex/rxjs";

interface LeafletControlButtonsProps {
    onSave: (text: string)=>void;
    showMyScansOnly?: boolean;
}

interface LeafletControlButtonsState {
    showMyScansOnly: boolean;
}

class LeafletControlButtons extends React.Component<LeafletControlButtonsProps, LeafletControlButtonsState> {


    constructor(props, context) {
        super(props, context);
        this.state = {
            showMyScansOnly: this.props.showMyScansOnly
        }
    }

    componentDidMount(){
        /*TODO do this the proper way with rxjs / observable*/
        let button = document.getElementById("showMyScansOnly");
        console.log(button);
        Observable.fromEvent(button, 'click')
            .subscribe(() => console.log('Clicked checkbox!'));
    }

    render() {
        return (
            <div className="goldenforms-pro">
                <div className="goldenforms-wrapper">
                    <div className="frm-row">
                        <label className="uit-option gblock">
                            <input ref="showMyScansOnly" checked={this.props.showMyScansOnly} type="checkbox" id="showMyScansOnly" name="showMyScansOnly"/>
                            <span className="checkbox-option"></span> Show my scans only
                        </label>
                    </div>
                </div>
            </div>
        );
    }
}

export default LeafletControlButtons;