import * as React from "react";
import {Map, TileLayer} from "react-leaflet";
import "../../style/ScanLocationsLeaflet.css";
import "../../style/LeafletControlButtons.css";
import {Coord, Scannable, ScanLocation} from "../model";
import LeafletMarker from "./LeafletMarker";
import config from "./../../config";
import "whatwg-fetch";
import {Observable} from "@reactivex/rxjs";
//import "rxjs/add/observable/fromEvent";
//import "rxjs/add/operator/throttle";
import fetchScannables from "../api/fetchScannables";

interface ScanLocationsLeafletProps {
    onSave: (text: string)=>void;
    text?: string;
    placeholder?: string,
    editing?: boolean,
    center?: Coord,
    scannables?: Scannable[];
    showMyScansOnly?: boolean;
}
interface ScanLocationsLeafletState {
    text?: string;
    center?: Coord;
    scannables?: Scannable[];
    showMyScansOnly?: boolean
}

class ScanLocationsLeaflet extends React.Component<ScanLocationsLeafletProps, ScanLocationsLeafletState> {

    constructor(props, context) {
        super(props, context);
        this.state = {
            text: this.props.text || '',
            center: this.props.center || new Coord(30, 0),
            scannables: this.props.scannables || [],
            showMyScansOnly: this.props.showMyScansOnly || false
        };
    }

    componentDidMount() {
        console.log('componentDidMount');
        /*TODO do this the proper way with rxjs / observable*/
        let button = document.getElementById("showMyScansOnly");
        var clicks = Observable.fromEvent(button, 'click');
        clicks
            .subscribe(
                x => {
                    let isChecked:boolean = x["target"].checked;
                    this.state.showMyScansOnly = isChecked;
                        let apiPostfix = '';
                    if (isChecked && window["fbId"]!==undefined){
                        apiPostfix = '?userid=' + window["fbId"] + '&limit=5000';
                    }else{
                        apiPostfix = '?limit=5000';
                    }
                    let facebookUserId: any = document.querySelector("#facebookUserId").getAttribute('value');
                    let source = fetchScannables(facebookUserId, apiPostfix);
                    this.subscribeToSource(source, apiPostfix);
                },
                e => console.log('onError: %s', e),
                () => console.log('onCompleted'));


        //var source = fetchScannables('?limit=5000');
        //this.subscribeToSource(source, retryApiConnection, '?limit=5000');
    }

    /*
    * if (window["fbId"]) {

      ;

     }
    * */

    subscribeToSource(source: any, apiPostfix: string) {
        let newScannables: Scannable[] = [];
        let oneSubscription = source.subscribe({
            next: x => {
                if (x) {
                    console.log(x.length);
                    x.forEach(s => {
                        let newScannable = new Scannable(s);
                        if (newScannable.scanLocation.latitude === 0 && newScannable.scanLocation.longitude === 0){
                            newScannable.scanLocation = new ScanLocation({latitude:0.00, longitude:0.0});
                        }
                        newScannables.push(newScannable);
                    });
                }
            },
            error: e => {
                console.error('connection error');
                console.error(e);
            },
            complete: () => {
                console.log(newScannables.length);
                this.setState((prevState, props) => {
                    return {
                        scannables: newScannables
                    };
                });
            }
        });

    };


    handleSubmit(e) {
        console.log("handleSubmit");
        const text = e.target.value.trim();
        if (e.which === 13) {
            //this.props.onSave(text);
        }
    }

    handleChange(e) {
        console.log("handleChange");

        //this.setState({ text: e.target.value });
        //Rx.Observable.fromPromise(fetch('/users'));
    }

    handleBlur(e) {
        console.log("handleBlur");
        //this.props.onSave(e.target.value);
    }

    render() {
        console.log('render() ScanLocationsLeaflet');
        L.Icon.Default.imagePath = '_/asset/org.informedindividual.apprspctiv/images/leaflet/';
        let leafletMarkers = [];
        if (this.state.scannables) {
            this.state.scannables.forEach(s => {
                console.log(s);
                leafletMarkers.push(<LeafletMarker scannable={s}/>);
            });
        }
        return (
            <div>
                <div className="goldenforms-pro">
                    <div className="goldenforms-wrapper">
                        <div className="frm-row">
                            <label className="uit-option gblock">
                                <input ref="showMyScansOnly" checked={this.props.showMyScansOnly} type="checkbox"
                                       id="showMyScansOnly" name="showMyScansOnly"/>
                                <span className="checkbox-option"></span> Show my scans only
                            </label>
                        </div>
                    </div>
                </div>
                <Map center={this.state.center.toLatLang()} zoom={1}>
                    <TileLayer
                        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    />
                    {leafletMarkers}
                </Map>
            </div>
        );
    }
}


export default ScanLocationsLeaflet;
