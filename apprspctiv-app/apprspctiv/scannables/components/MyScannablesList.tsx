import * as React from "react";
import "../../style/MyScannablesList.css";
import { Opinion, Scannable } from "../model";
import fetchScannables from "../api/fetchScannables";
import fetchOpinions from "../api/fetchOpinions";
import createOpinion from "../api/createOpinion";
import "react-select/dist/react-select.min.css";
import "react-quill/dist/quill.snow.css";
import { connect } from "react-redux";
import ReactSelectClass = require("react-select");
import ReactQuill = require('react-quill');
import * as Dropzone from 'react-dropzone';
import * as Request from 'superagent';


interface MyScannablesListProps {
    onSave: (text: string) => void;
    myScannables?: Scannable[];
    myOpinions?: Opinion[];
    myOpinionOptions?: any;
    selected?: String[];
    categories?: any;
    category?: any;
    options?: any;
    facebookUserId?: any;
    selectedPlaceHolder?: string;
    opinionsPlaceHolder?: string;
    categoryPlaceHolder?: string;
    opinionId?: number;
    opinionScore?: number;
    opinionText?: string;
    opinionTitle?: string;
    opinionDescription?:string;
    opinionImageUrls?:string[];
    prspctivTitle?: string;
    uploadedFileCloudinaryPublicId?:string;
    uploadedFileCloudinaryUrl?:string;
    uploadedFile?:any;
}

interface MyScannablesListState {
    myScannables?: Scannable[];
    myOpinions?: Opinion[];
    myOpinionOptions?: any;
    selected?: String[];
    options?: any;
    categories?: any;
    category?: any;
    facebookUserId?: any;
    selectedPlaceHolder?: string;
    opinionsPlaceHolder?: string;
    categoryPlaceHolder?: string;
    opinionId?: number;
    opinionScore?: number;
    opinionText?: string;
    opinionTitle?: string;
    opinionDescription?: string;
    opinionImageUrls?:string[];
    prspctivTitle?: string;
    uploadedFileCloudinaryPublicId?:string;
    uploadedFileCloudinaryUrl?:string;
    uploadedFile?:any;
}

/*https://css-tricks.com/image-upload-manipulation-react/*/
const CLOUDINARY_UPLOAD_PRESET = 'alzq0au5';
const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/informedindividual/upload';

class MyScannablesList extends React.Component<MyScannablesListProps, MyScannablesListState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            myScannables: this.props.myScannables || [],
            options: [],
            selected: [],
            categories: [{ value: 'ethics', label: 'ethics' }, { value: 'environment', label: 'environment' }, { value: 'taste', label: 'taste' }],
            category: '',
            selectedPlaceHolder: 'Please wait, loading ...',
            categoryPlaceHolder: 'Ready to select',
            opinionsPlaceHolder: 'Loading your opinions',
            opinionId: -1,
            opinionScore: 0.4,
            opinionText: '',
            opinionTitle: '',
            opinionDescription: '',
            opinionImageUrls:[],
            prspctivTitle: '',
            myOpinions: [],
            myOpinionOptions: [],
            uploadedFileCloudinaryPublicId:'',
            uploadedFileCloudinaryUrl:'',
            uploadedFile:''
        }

    }

    componentDidMount() {
        let retryApiConnection: boolean = true;

        let setFacebookId = function(event) {
            let apiPostfix: string = 'limit=5000';

            let facebookUserId: any = document.querySelector("#facebookUserId").getAttribute('value');
            if (facebookUserId !== undefined) {
                this.setState({ facebookUserId: facebookUserId });
            }
            let scannablesSource: any = fetchScannables(facebookUserId, apiPostfix);

            this.subscribeToSource(scannablesSource, apiPostfix);
            this.getOpinions("");

        };

        document.querySelector("#facebookUserId").addEventListener("change", setFacebookId.bind(this), false);
    }

    getOpinions(){
        let opinionsSource: any = fetchOpinions(this.state.facebookUserId,"");
        this.subscribeToOpinions(opinionsSource);
    }

    subscribeToOpinions(source: any) {
        let userOpinions: Opinion[] = [];
        let newOptions: any = [];
        let oneSubscription = source.subscribe({
            next: x => {
                if (x && x.length) {
                    x.forEach(s => {
                        let newOpinion = new Opinion(s);
                        let option = {
                            value: newOpinion.id,
                            label: newOpinion.title
                        };
                        newOptions.push(option);
                        userOpinions.push(newOpinion);
                    });
                }
            },
            error: e => {
                console.error('connection error');
                console.error(e);
            },
            complete: () => {
                this.setState({ myOpinions: userOpinions, myOpinionOptions: newOptions, opinionsPlaceHolder: 'Opinions loaded, click to select' });
            }
        });
    };

    subscribeToSource(source: any, apiPostfix: string) {
        let newScannables: Scannable[] = [];
        let newOptions: any = [];
        let oneSubscription = source.subscribe({
            next: x => {
                if (x && x.length) {
                    x.forEach(s => {
                        let newScannable = new Scannable(s);
                        let option = {
                            value: newScannable.gtin,
                            label: newScannable.gtin
                        };
                        if (newScannable.product && newScannable.product.title){
                          option.label = newScannable.product.title;
                        }
                        newOptions.push(option);
                    });
                }
            },
            error: e => {
                console.error('connection error');
                console.error(e);
            },
            complete: () => {
                this.setState({ options: newOptions, selectedPlaceHolder: 'Scans loaded, click to select' });
            }
        });
    };

    handleRating(e) {
        this.setState({ opinionScore: e.target.value });
    }

    onImageDrop(files) {
        console.log(files);
        console.log(files[0]);
        this.setState({
            uploadedFile: files[0]
        });

        this.handleImageUpload(files[0]);
    }

    handleImageUpload(file) {
        console.log('handleImageUpload:' + file);
        if (!file){
            return;
        }
        console.log('post to ' + CLOUDINARY_UPLOAD_URL);
        let upload = Request.post(CLOUDINARY_UPLOAD_URL)
            .field('upload_preset', CLOUDINARY_UPLOAD_PRESET)
            .field('file', file);

        upload.end((err, response) => {
            if (err) {
                console.error(err);
            }

            if (response.body.secure_url !== '') {
                this.setState({
                    uploadedFileCloudinaryUrl: response.body.secure_url,
                    opinionImageUrls:[response.body.secure_url]
                });
                console.log(response.body);
            }
            console.log(this.state.uploadedFileCloudinaryUrl);
        });
    }

    handleSelected(e) {
        this.setState({ selected: e });
    }

    handleOpinions(e) {
        this.setState({ myOpinionOptions: e });
    }

    handleOpinionClick(e) {
        let clickedOpinion = this.state.myOpinions.filter(opinion => opinion.id === e.value)[0];
        let products = [];
        if (clickedOpinion.products) {
            clickedOpinion.products.forEach(product => {
                products.push({value:product.gtin, label:product ? product.title : product.gtin});
            });
        }
        this.setState({
            opinionId: clickedOpinion.id,
            opinionScore: clickedOpinion.score,
            opinionTitle: clickedOpinion.title,
            opinionDescription: clickedOpinion.description,
            category: clickedOpinion.category,
            selected: products});
    }

    setOpinionTitle(e) {
        this.setState({ opinionTitle: e.target.value});
    }

    handleCategory(e) {
        console.log(e);
        this.setState({ category: e });
    }

    addOpinion(e) {
        let form = document.querySelector("#prspctivForm");
        let apiPostFix = `?userid=${this.state.facebookUserId}`;
        apiPostFix += `&category=${this.state.category.value}`;
        apiPostFix += `&title=${this.state.opinionTitle}`;
        apiPostFix += `&description=${this.state.opinionDescription}`;
        apiPostFix += `&score=${this.state.opinionScore}`;

        this.state.selected.forEach(product => {
            apiPostFix += `&gtin=${product["value"]}`;
        });

        let source: any = createOpinion(this.state.opinionId, apiPostFix, form);
        this.setOpinionOnReturn(source);

    }

    setOpinionOnReturn(source: any) {
        let test = source.subscribe({
            next: x => {
                console.log("next" + x);
                if (x && x.length) {
                    x.forEach(s => {
                        console.log(s);
                    });
                }
            },
            error: e => {
                console.error('connection error');
                console.error(e);
            },
            complete: () => {
                this.setState({opinionsPlaceHolder: 'Refresh opinions...'});
                this.getOpinions();
            }
        });
    };

    handleOpinionDescription(e){
      this.setState({ opinionDescription: e });
    }

    savePrspctiv(e) {
        console.log('savePrspctiv');
        console.log(e);
    }

    updateState(newValue) {
        console.log('state : ' + this.state);
    }

    render() {

        return (
            <div className="MyScannablesListWrapper">
                <form id="prspctivForm" name="prspctivForm" method="post" encType="application/x-www-form-urlencoded">
                    {/*encType="multipart/form-data"*/}
                    <div className="goldenforms-pro">
                        <div className="goldenforms-wrapper">
                            <div className="frm-section colm colm6">
                                <label htmlFor="opinion-title" className="field-label">Opinion title</label>
                                <label className="field">
                                    <input onChange={this.setOpinionTitle.bind(this)} value={this.state.opinionTitle} name="title" type="text" id="opinion-title" className="uit-input" placeholder="Give this opinion a title" />
                                </label>
                            </div>
                            <div className="frm-section colm colm6">
                                <label className="field-label">Opinion category</label>
                                <label className="field">
                                    <ReactSelectClass.Creatable
                                        options={this.state.categories}
                                        placeholder={this.state.categoryPlaceHolder}
                                        value={this.state.category}
                                        name="category"
                                        promptTextCreator={(value) => `Submit new opinion category "${value}"`}
                                        onChange={this.handleCategory.bind(this)}
                                    />
                                </label>
                                <span className="text-hint">
                                    Select - or create - one single category for your opinion. This is usually an area of
                                        expertice where friends comes to you for advice. Politics, tech, environment, taste?
                                </span>
                            </div>
                            <div className="frm-section colm colm6">
                                <div className="uit-rating-wrapper">
                                    <span> Opinion score and optional description</span>
                                    <div className="frm-row">
                                        <div className="frm-section colm colm6"></div>
                                        <div id="prspctiv-rating" className="uit-rating">
                                        
                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a5" name="opinionScore" value="1" checked={this.state.opinionScore == 1} />
                                            <label className="sta-full" htmlFor="star-a5"><span>6 Stars</span></label>

                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a4" name="opinionScore" value="0.8" checked={this.state.opinionScore == 0.8} />
                                            <label className="sta-full" htmlFor="star-a4"><span>5 Stars</span></label>

                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a3" name="opinionScore" value="0.6" checked={this.state.opinionScore == 0.6} />
                                            <label className="sta-full" htmlFor="star-a3"><span>4 Stars</span></label>

                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a2" name="opinionScore" value="0.4" checked={this.state.opinionScore == 0.4} />
                                            <label className="sta-full" htmlFor="star-a2"><span>3 Stars</span></label>

                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a1" name="opinionScore" value="0.2" checked={this.state.opinionScore == 0.2} />
                                            <label className="sta-full" htmlFor="star-a1"><span>2 Stars</span></label>

                                            <input onChange={this.handleRating.bind(this)} type="radio" id="star-a0" name="opinionScore" value="0" checked={this.state.opinionScore == 0} />
                                            <label className="sta-full" htmlFor="star-a0"><span>1 Star</span></label>
                                            <input type="hidden" name="score" value={this.state.opinionScore}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="frm-section">
                                <label className="field">
                                    {/*https://react-dropzone.netlify.com/#proptypes*/}
                                    <Dropzone
                                        onDrop={this.onImageDrop.bind(this)}
                                        accept="image/*">
                                        <p>Drop an image or click to select a file to upload.</p>
                                    </Dropzone>
                                </label>
                            </div>
                            <div className="frm-section uploadedFileCloudinaryPreview">
                                <label className="field">
                                    {this.state.uploadedFileCloudinaryUrl === '' ? null :
                                        <div>
                                            <p>{this.state.uploadedFile.name}</p>
                                            <img src={this.state.uploadedFileCloudinaryUrl} />
                                            <input type="hidden" name="imageUrls" value={this.state.opinionImageUrls}/>
                                        </div>}
                                </label>
                            </div>
                            <div className="frm-section">
                                <label className="field">
                                    <ReactQuill name="opinionDescription" onChange={this.handleOpinionDescription.bind(this)} id="opinionDescription" value={this.state.opinionDescription} />
                                    <input type="hidden" name="description" value={this.state.opinionDescription}/>
                                    {/*<span className="text-hint">
                                        <strong>Hint:</strong> Don't be negative - Just be awesome!
                                    </span>*/}
                                </label>
                            </div>
                            <div className="frm-section colm colm6">
                                <label className="field-label">Add this opinion to products</label>
                                <label className="field">
                                    <ReactSelectClass
                                        name="products"
                                        multi
                                        joinValues
                                        options={this.state.options}
                                        placeholder={this.state.selectedPlaceHolder}
                                        value={this.state.selected}
                                        onChange={this.handleSelected.bind(this)}
                                    />
                                </label>
                                <span className="text-hint">
                                    We have pre-loaded your latest scans with our Prspctiv mobile scanner, add one or more
                                    of them to your opinion here. Your opinion will be related to each selected product for
                                    everyone to see.
                                </span>
                            </div>
                            <div className="frm-section colm colm6">
                                <button onClick={this.addOpinion.bind(this)} type="button" className="uit-button btn-green"> Save opinion</button>
                            </div>
                            <div className="frm-section colm colm6">
                                <label htmlFor="prspctiv-name" className="field-label">Prspctiv title</label>
                                <label className="field">
                                    <input type="text" id="prspctiv-title" className="uit-input" placeholder="Name this prspctiv" />
                                </label>
                            </div>
                            <div className="frm-section colm colm6">
                                <label className="field-label">Your opinions</label>
                                <label className="field">
                                    <ReactSelectClass
                                        multi
                                        joinValues
                                        has-value
                                        onValueClick={this.handleOpinionClick.bind(this)}
                                        options={this.state.myOpinionOptions}
                                        placeholder={this.state.opinionsPlaceHolder}
                                        value={this.state.myOpinionOptions}
                                        onChange={this.handleOpinions.bind(this)}
                                    />
                                </label>
                                <span className="text-hint">
                                    We have pre-loaded your latest scans with our Prspctiv mobile scanner, add one or more
                                    of them to your opinion here. Your opinion will be related to each selected product for
                                    everyone to see.
                                </span>
                            </div>
                            <div className="frm-section colm colm6">
                                <button onClick={this.savePrspctiv.bind(this)} type="button" className="uit-button btn-blue"> Create Prspctiv</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
const mapStateToProps = state => ({});
export default connect(mapStateToProps)(MyScannablesList);
