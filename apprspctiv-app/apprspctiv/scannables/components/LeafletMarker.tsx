import * as React from "react";
import {Marker, Popup} from "react-leaflet";
import {Scannable} from "../model";
import Element = JSX.Element;

interface LeafletMarkerProps {
    scannable?: Scannable;
}

interface LeafletMarkerState {
    scannable: Scannable;
}

class LeafletMarker extends React.Component<LeafletMarkerProps, LeafletMarkerState> {

    static existingMarkerPos:[string] = ['0'];

    constructor(props, context) {
        super(props, context);
        this.state = {
            scannable: this.props.scannable
        }
    }

    render() {
        console.log('render()' + this.state.scannable);
        LeafletMarker.existingMarkerPos = ['0'];
        let product,scanLocation,lastScanned = '';
        let scanCount:number = 0;
        let productImage:Element;
        if (this.state.scannable.product){
            product = this.state.scannable.product.title + ' ' + this.state.scannable.product.subtitle;
            if (this.state.scannable.product.imageUrl){
                productImage = <img className='leafletImagePopup' src={this.state.scannable.product.imageUrl}/>;
            }
        }
        if (this.state.scannable.scanLocation){
            scanLocation = this.state.scannable.scanLocation.city + ', ' + this.state.scannable.scanLocation.country;
            lastScanned = this.state.scannable.scanLocation.lastScan;
            scanCount = this.state.scannable.scanLocation.scanCount;
        }

        let latitude = this.state.scannable.scanLocation.latitude;
        let longitude = this.state.scannable.scanLocation.longitude;
        if (LeafletMarker.existingMarkerPos.indexOf(String(latitude)+String(longitude))){
            latitude+=(Math.floor(Math.random() * 10) + 1)/10000;
            longitude+=(Math.floor(Math.random() * 10) + 1)/10000;
            LeafletMarker.existingMarkerPos.push(String(latitude)+String(longitude));
        }else{
            LeafletMarker.existingMarkerPos.push(String(latitude)+String(longitude));
        }

        return (
            <Marker position={[latitude, longitude]}>
                <Popup>
                    <span className="popupWrapper">
                        <h5>Scanned GTIN: {this.state.scannable.gtin}</h5>
                        {productImage}
                        <div><span className="popupProperty">Attached product:</span></div>
                        <div>{product}</div>
                        <div><span className="popupProperty">Location:</span></div>
                        <div>{scanLocation}</div>
                        <div>Latitude:{this.state.scannable.scanLocation.latitude}</div>
                        <div>Longitude:{this.state.scannable.scanLocation.longitude}</div>
                        <div>{scanCount} scans at location</div>
                        <div>Last scan:{lastScanned}</div>
                    </span>
                </Popup>
            </Marker>
        );
    }
}

export default LeafletMarker;