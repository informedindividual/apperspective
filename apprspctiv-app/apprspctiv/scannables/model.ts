interface IUserParams {
    userid?: string;
};

export class User {
    userid: string;

    constructor(params: IUserParams) {
        this.userid = params.userid;
    }
};

interface IScannableParams {
    id?: number;
    gtin?: string;
    product?: Product;
    scanLocation?: ScanLocation;
};

export class Scannable {
    constructor(params: IScannableParams) {
        this.id = params.id;
        this.gtin = params.gtin;
        if (params.product) {
            this.product = new Product(params.product);
        }
        if (params.scanLocation) {
            this.scanLocation = new ScanLocation(params.scanLocation);
        }
    }

    id: number;
    gtin: string;
    product: Product;
    scanLocation: ScanLocation;

    toLatLang() {
        if (this.scanLocation) {
            return <[number, number]>
                this.scanLocation.toLatLang();
        }
    }
};

interface IOpinionParams {
    id?: number;
    title?: string;
    description?: string;
    score?: number;
    category?: string;
    products?: Product[];
};

export class Opinion {
    id: number;
    title: string;
    description: string;
    score: number;
    category: string;
    products: Product[];

    constructor(params: IOpinionParams) {
        this.id = params.id;
        this.title = params.title;
        this.description = params.description;
        this.score = params.score;
        this.category = params.category;
        this.products = params.products;
    }
};

interface IProductParams {
    id?: number;
    gtin?: string;
    title?: string;
    subtitle?: string;
    imageUrl?: string;
};

export class Product {
    id: number;
    gtin: string;
    title: string;
    subtitle: string;
    imageUrl: string;

    constructor(params: IProductParams) {
        this.id = params.id;
        this.gtin = params.gtin;
        this.title = params.title;
        this.subtitle = params.subtitle;
        this.imageUrl = params.imageUrl;
    }
};

interface IScanLocationParams {
    id?: number;
    country?: string;
    zipCode?: string;
    city?: string;
    countryCode?: string;
    regionName?: string;
    timeZone?: string;
    lastScan?: string;
    scanCount?: number;
    latitude?: number;
    longitude?: number;
};

export class ScanLocation {
    id: number;
    country: string;
    zipCode: string;
    city: string;
    countryCode: string;
    regionName: string;
    timeZone: string;
    lastScan: string;
    scanCount: number;
    latitude?: number;
    longitude?: number;

    constructor(params: IScanLocationParams) {
        this.id = params.id;
        this.country = params.country;
        this.zipCode = params.zipCode;
        this.city = params.city;
        this.countryCode = params.countryCode;
        this.regionName = params.regionName;
        this.timeZone = params.timeZone;
        this.lastScan = params.lastScan;
        this.scanCount = params.scanCount;
        this.latitude = params.latitude;
        this.longitude = params.longitude;
    }

    toLatLang() {
        if (this.latitude && this.longitude) {
            return <[number, number]> [this.latitude, this.longitude];
        }
    }
}

export type IState = Scannable[];

export class Coord {
    lat: number;
    lng: number;

    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }

    toLatLang() {
        return <[number, number]> [this.lat, this.lng];
    }
}
