import { assign } from 'lodash';
import { handleActions, Action } from 'redux-actions';

import { Scannable, IState } from './model';
import {
    EDIT_SCANNABLE
} from './actions';

const initialState: IState = [];

export default handleActions<IState, Scannable>({
  [EDIT_SCANNABLE]: (state: IState, action: Action<Scannable>): IState => {
    return <IState>state.map(scannable =>
        scannable.id === action.payload.id
        ? assign(<Scannable>{}, scannable, { gtin: action.payload.gtin })
        : scannable
    );
  }
}, initialState);
