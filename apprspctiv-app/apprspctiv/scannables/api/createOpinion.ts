import config from "./../../config";
import "whatwg-fetch";
import {Observable} from "@reactivex/rxjs";

export default function(opinionId:number, apiPostfix:string, form:any) {
    /*return Observable.create(function (observer){

        fetch(config.apiService + "/testpost",{
            method:'POST',
            mode: 'no-cors',
            body: new FormData(form)
        })
        .then(res => res.json())
        .then(j => {
            observer.next(j);
            observer.complete();
        })
        .catch(observer.onError);
    });*/

    let apiService = config.apiService + '/productOpinion';
    if (opinionId !== undefined && opinionId !== -1){
        apiService+="/"+opinionId
    }
    apiService+=apiPostfix;
    let source = Observable.create(function (observer) {
        fetch(apiService,{
            method: 'POST',
            mode: 'no-cors',
            headers: {
              'Ocp-Apim-Subscription-Key': config.apiSubscriptionKey
            },
            body: new FormData(form)
        })
        //.then(res => res.json())
        .then(j => {
            observer.next(j);
            observer.complete();
        }).catch(observer.onError);
    });
    return source;
};
