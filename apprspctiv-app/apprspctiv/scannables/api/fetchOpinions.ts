import config from "./../../config";
import "whatwg-fetch";
import {Observable} from "@reactivex/rxjs";

export default function(userid, apiPostfix) {
    let apiService = config.apiService + '/opinions/'+userid+apiPostfix;
    let source = Observable.create(function (observer) {
        fetch(apiService,{
            method: "GET",
            headers: {
              'Ocp-Apim-Subscription-Key': config.apiSubscriptionKey
            }
          })
          .then(res => res.json())
          .then(j => {
              observer.next(j);
              observer.complete();
          }).catch(observer.onError);
    });
    return source;
};
