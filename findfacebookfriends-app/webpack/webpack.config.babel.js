/* global __dirname */

import path from 'path';
import webpack from 'webpack';
import webpackConfig from './_base';

webpackConfig.devtool = 'source-map';

webpackConfig.output = {
  path: path.resolve(__dirname, '../', (process.env.PROD == 'true') ? '../src/main/resources/assets/apps/findfacebookfriends' :'build' ),
  filename: 'findfacebookfriends.js'
};

webpackConfig.plugins = [
  new webpack.optimize.UglifyJsPlugin({
    output: {
      comments: false
    },
    minimize: true,
    compress: {
      warnings: false
    }
  })
];

export default webpackConfig;
