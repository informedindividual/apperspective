/* global FB */
import React, {Component} from "react";
import Profile from "./Profile";
import ErrMsg from "./ErrMsg";
import Login from "./Login";
import config from "../../config";
import localConfig from "../../localConfig";
import emitter from "../utils/emitter";
import {getData} from "../utils/util";
import "../style/App.css";
import "../style/spinner-pulse.css";
import {apiConfig} from "../utils/apiConfig";

type
AppState = {
    status: string,
    profile? : Object,
    myFriends? : Object,
    query? : string
};

class App extends Component {

    state: AppState = {
        status: 'loading'
    };

    componentWillMount() {

    }

    componentWillUnmount() {
        emitter.removeListener('search');
    }

    componentDidMount() {
        emitter.on('search', query => this.setState({query}));
        window.fbAsyncInit = () => {
          if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
            FB.init(localConfig);
          }else{
            FB.init(config);
          }


            // show login
            FB.getLoginStatus(
                response => response.status !== 'connected' && this.setState({status: response.status})
            );

            FB.Event.subscribe('auth.authResponseChange', (response) => {
                // start spinner
                this.setState({status: 'loading'});

                (async() => {
                    try {
                        const {profile, myFriends} = await getData();
                        this.setState({status: response.status, profile, myFriends});
                        //window["fbId"] = response.authResponse.userID;
                        //window["fbProfile"] = profile;
                        //window["fbFriends"] = myFriends;
                        document.querySelector("#facebookUserId").value = response.authResponse.userID;
                        document.querySelector("#facebookUserId").dispatchEvent(new Event('change'));
                    } catch (e) {
                        this.setState({status: 'err'});
                    }
                })();
            });
        };

        apiConfig();
    }

    _click(): void {
        FB.login(_ => {
        }, {scope: ['user_posts', 'user_friends']});
    }

    mainRender() {
        const {profile, myFriends, status, query} = this.state;
        if (status === 'err') {
            return (<ErrMsg />);
        } else if (status === 'unknown' || status === 'not_authorized') {
            return <Login fBLogin={this._click}/>;
        } else if (status === 'connected') {
            return (
                <div className="app-wrapper">
                    <div className="flexContainer">
                        <div className="profileStyle">
                            <div className="headerStyle">
                                <Profile {...profile} />
                                {/*<FriendList myFriends={myFriends} query={query} />*/}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="spinner-wrapper">
                <div className="sk-pulse"/>
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.mainRender()}
            </div>
        );
    }

}

export default App;
