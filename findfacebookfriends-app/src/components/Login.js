// @flow

import React, { PropTypes } from 'react';
import '../style/Login.css';

const Login = ({ fBLogin }: { fBLogin: Function }) => (
  <div className="wrapper">
    <div>
      <h2 className="title">Log in with Facebook</h2>
      <div className="main">
        <h4>Facebook requires your permission to use this application</h4>
        <button className="pure-button btn" onClick={fBLogin}>Agree</button>
      </div>
    </div>
  </div>
);

export default Login;
