var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf')
};

exports.get = handleGet;
exports.post = handleGet;

function handleGet(req) {
    var appId = libs.portal.getComponent().config.appId;
    var view = resolve('app.html');
    var model = {
        appId: appId,
        apiService: libs.portal.serviceUrl({service: 'api'}),
        wsService: libs.portal.serviceUrl({service: 'ws'})
    };

    var appJs = libs.portal.assetUrl({
        path: 'apps/'+appId+'/'+appId+'.js'
    });

    var appCss = libs.portal.assetUrl({
        path: 'apps/'+appId+'/'+appId+'.css'
    });

    return {
        body: libs.thymeleaf.render(view, model),
        pageContributions: {
            headEnd: [
                "<link rel='stylesheet' href='"+appCss+"'/>"
            ],
            bodyEnd: [
                '<script src="' + appJs + '" type="text/javascript"></script>'
            ]
        }
    };
}