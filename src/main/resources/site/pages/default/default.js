var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    thymeleaf: require('/lib/xp/thymeleaf'),
};

exports.get = handleGet;
exports.post = handleGet;

function handleGet(req) {
    var view = resolve('default.html');
    var content = libs.portal.getContent();;
    var model = {
        mainRegion: content.page.regions['main']
    };

    var appJs = libs.portal.assetUrl({
        path: 'js/apprspctiv.js'
    });

    /*var jquery = libs.portal.assetUrl({
        path: 'libs/jquery/jquery-1.9.1.min.js'
    });

    var jqueryMaskedInput = libs.portal.assetUrl({
        path: 'libs/jquery/jquery.maskedinput.min.js'
    });*/

    var fontawesomeCss = libs.portal.assetUrl({
        path: 'css/font-awesome.min.css'
    });

    var goldenformsProCss = libs.portal.assetUrl({
        path: 'css/goldenforms-pro.css'
    });

    var appCss = libs.portal.assetUrl({
        path: 'css/apprspctiv.css'
    });

    return {
        body: libs.thymeleaf.render(view, model),
        pageContributions: {
            headEnd: [
                "<link rel='stylesheet' href='"+appCss+"'/>",
                "<link rel='stylesheet' href='"+fontawesomeCss+"'/>",
                "<link rel='stylesheet' href='"+goldenformsProCss+"'/>",
                /*'<script src="' + jquery + '" type="text/javascript"></script>',
                '<script src="' + jqueryMaskedInput + '" type="text/javascript"></script>',*/
            ],
            bodyEnd: [
            ]
        }
    };
}
