#Prspctiv application
In prod it is here: https://prspctiv.informedindividual.org/
For now you will need to give your facebook username to rune.forberg@informedindividual.org
so he can add you as a test user, if you want to test development version locally

##Installing node / npm / nvm
The app is tested with following versions of node / npm:
node: v7.6.0
npm: 4.1.2

https://nodejs.org/en/download/

Pro tip: If you are not on windows, recommended approach is to install 
nvm (node version manager) from https://github.com/creationix/nvm to 
manage node / npm versions and easily swich back and forth between versions.

##Run dev server
Run commands inside top folder apprspctiv

###Initial install
`npm install`

###Use for development
`npm start`

###Use for production
`npm build`
